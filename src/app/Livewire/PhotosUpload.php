<?php

namespace App\Livewire;

use App\Livewire\Forms\CatalogForm;
use App\Models\Catalog;
use App\Models\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Drivers\Gd\Driver;
use Intervention\Image\ImageManager;
use Livewire\Component;
use Livewire\Features\SupportFileUploads\TemporaryUploadedFile;
use Livewire\WithFileUploads;

class PhotosUpload extends Component
{
    use WithFileUploads;

    public const HASH_ALGO = 'sha256';

    public CatalogForm $form;
    public array $files = [];

    public function mount(Catalog $catalog): void
    {
        $this->form->setCatalog($catalog);
    }

    public function render()
    {
        return view('livewire.photos-upload');
    }

    public function updatedFiles(): void
    {
        /** @var TemporaryUploadedFile $file */
        $errors = [];
        $success = [];
        $successCount = 0;
        foreach ($this->files as $file){
            if(str_starts_with($file->getMimeType(), 'image')) {
                $fileName = hash_file(self::HASH_ALGO, $file->path());
                $ext = $file->guessExtension();
                if ($ext) {
                    $fileName = sprintf("%s.%s", $fileName, $ext);
                }
                $status = Storage::disk('public')->putFileAs(env('PHOTO_PATH'), $file->path(), $fileName);
                if ($status === false) {
                    $errors[] = sprintf("Failed to process file %s", $file->getClientOriginalName());
                } else {
                    Storage::disk('public')->makeDirectory(env('PHOTO_THUMBNAIL_PATH'));
                    $image = new ImageManager(Driver::class);
                    $img = $image->read(Storage::disk('public')->path(sprintf("%s/%s", env('PHOTO_PATH'), $fileName)));
                    $img->cover(env('PHOTO_THUMBNAIL_SIZE'), env('PHOTO_THUMBNAIL_SIZE'));
                    $img->save(sprintf("%s/%s", Storage::disk('public')->path(env('PHOTO_THUMBNAIL_PATH')), $fileName));
                    $successCount++;
                    $photo = DB::table('photos')
                        ->where('image', $fileName)
                        ->where('user_id', Auth::id())
                        ->where('catalog_id', $this->form->catalog->id)
                        ->first();
                    if(!$photo){
                        $photo = new Photo();
                        $photo->name = $file->getClientOriginalName();
                        $photo->image = $fileName;
                        $photo->user_id = Auth::id();
                        $photo->catalog_id = $this->form->catalog->id;
                        $photo->file_name = $file->getClientOriginalName();
                        $photo->file_size = $file->getSize();
                        $photo->save();
                    }
                }
            }else{
                $errors[] = sprintf("File type is not supported %s", $file->getClientOriginalName());
            }
        }
        if($successCount > 0){
            $plural = $successCount > 1 ? 's' : '';
            $success[] = sprintf(
                "Successfully processed %d file%s, saved to catalog %s",
                $successCount,
                $plural,
                $this->form->catalog->name
            );
        }
        Session::flash('errors', $errors);
        Session::flash('success', $success);
    }
}
