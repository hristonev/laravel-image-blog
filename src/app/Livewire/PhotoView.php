<?php

namespace App\Livewire;

use App\Livewire\Forms\PhotoForm;
use App\Models\Photo;
use Livewire\Component;

class PhotoView extends Component
{
    public PhotoForm $form;

    public function mount(Photo $photo): void
    {
        $this->form->setPhoto($photo);
    }

    public function updated()
    {
        $this->form->update();
    }

    public function render()
    {
        return view('livewire.photo-view');
    }
}
