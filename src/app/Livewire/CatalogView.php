<?php

namespace App\Livewire;

use App\Livewire\Forms\CatalogForm;
use App\Models\Catalog;
use JetBrains\PhpStorm\NoReturn;
use Livewire\Component;

class CatalogView extends Component
{
    public CatalogForm $form;

    public function mount(Catalog $catalog): void
    {
        $this->form->setCatalog($catalog);
    }

    public function updated()
    {
        $this->form->update();
        $this->dispatch('catalog-updated', ['name' => $this->form->name]);
    }

    public function render()
    {
        return view('livewire.catalog-view');
    }
}
