<?php

namespace App\Livewire\Forms;

use App\Models\Catalog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Livewire\Form;

class CatalogForm extends Form
{
    public ?Catalog $catalog;
    public string $name = '';

    public function rules()
    {
        return [
            'name' => [
                'required',
                'min:3',
                Rule::unique('catalogs', 'name')->where(fn ($query) => $query->where('user_id', Auth::id()))
            ]
        ];
    }

    public function setCatalog(Catalog $catalog): static
    {
        $this->catalog = $catalog;
        $this->name = $catalog->name;
        return $this;
    }

    public function store()
    {
        $this->validate();
        $catalog = new Catalog();
        $catalog->name = $this->name;
        $catalog->user_id = Auth::id();
        $catalog->save();
        $this->reset();
    }

    public function update()
    {
        if(Auth::user()->cannot('update', $this->catalog)){
            abort(403);
        }
        try {
            $this->validate();
        }catch (ValidationException $exception){
            $this->name = $this->catalog->name;
            throw $exception;
        }
        $this->catalog->update($this->all());
    }
}
