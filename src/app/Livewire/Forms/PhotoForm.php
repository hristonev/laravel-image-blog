<?php

namespace App\Livewire\Forms;

use App\Models\Catalog;
use App\Models\Photo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Livewire\Form;

class PhotoForm extends Form
{
    public ?Photo $photo;
    public string $name = '';

    public function setPhoto(Photo $photo): static
    {
        $this->photo = $photo;
        $this->name = $photo->name;
        return $this;
    }

    public function update()
    {
        if(Auth::user()->cannot('update', $this->photo)){
            abort(403);
        }

        $this->photo->update($this->all());
    }
}
