<?php

namespace App\Livewire;

use App\Livewire\Forms\CatalogForm;
use App\Models\Catalog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Livewire\Attributes\Validate;
use Livewire\Component;
use Livewire\WithPagination;

class CatalogList extends Component
{
    use WithPagination;

    public CatalogForm $form;

    public function save(): void
    {
        $this->form->validate();
        $this->form->store();
    }

    public function delete($id)
    {
        if(Auth::user()->cannot('delete', Catalog::find($id))){
            abort(403);
        }
        Catalog::destroy([$id]);
    }

    public function render()
    {
        return view('livewire.catalog-list', [
            'catalogs' => DB::table('catalogs')
                ->selectRaw('catalogs.name, catalogs.id, COUNT(photos.id) as photos')
                ->where('catalogs.user_id', Auth::id())
                ->leftJoin('photos', 'catalogs.id', '=', 'photos.catalog_id')
                ->groupBy('catalogs.id', 'catalogs.name')
                ->orderBy('name')
                ->paginate(10)
        ]);
    }
}
