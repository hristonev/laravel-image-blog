<?php

namespace App;

use App\Enum\PaginatorEnrichType;
use App\Models\Photo;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;

class Helpers
{
    private static function thumbnailEnrichType($data, $property)
    {
        $data->thumbnail = Storage::url(sprintf("%s/%s", env('PHOTO_THUMBNAIL_PATH'), $data->$property));
        return $data;
    }

    public static function enrichThumbnailToPaginator(LengthAwarePaginator $paginator, PaginatorEnrichType $type, string $property = 'image'): LengthAwarePaginator
    {
        return tap($paginator, fn($pi) => $pi->getCollection()->transform(fn($i) => match($type){
            PaginatorEnrichType::THUMBNAIL => self::thumbnailEnrichType($i, $property)
        }));
    }

    public static function photoPath(Photo $photo)
    {
        return Storage::url(sprintf("%s/%s", env('PHOTO_PATH'), $photo->image));
    }
}
