<?php

namespace App\Http\Controllers;

use App\Enum\PaginatorEnrichType;
use App\Helpers;
use App\Models\Catalog;
use App\Models\Photo;
use App\Models\PhotoComment;
use App\Models\User;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;

class PhotoController extends Controller
{
    private const ITEMS_PER_PAGE = 12;

    private function getBuilder(): Builder
    {
        return DB::table('photos')
            ->select('photos.*', 'users.name as author', 'catalogs.name as catalog')
            ->join('users', 'users.id', '=', 'photos.user_id')
            ->join('catalogs', 'catalogs.id', '=', 'photos.catalog_id')
            ->orderBy('photos.created_at', 'desc');
    }

    private function paginate(Builder $builder, int $numItems = self::ITEMS_PER_PAGE): LengthAwarePaginator
    {
        return Helpers::enrichThumbnailToPaginator(
            $builder->paginate($numItems),
            PaginatorEnrichType::THUMBNAIL
        );
    }

    public function adminList(Catalog $catalog)
    {
        return view('catalog.photos', [
            'catalog' => $catalog,
            'photos' => $this->paginate(
                $this->getBuilder()
                    ->where('photos.user_id', '=', Auth::id())
                    ->where('photos.catalog_id', '=', $catalog->id)
            ),
        ]);
    }

    public function list()
    {
        return view('photo-list', [
            'photos' => $this->paginate($this->getBuilder()),
        ]);
    }

    public function listByUser(User $user)
    {
        return view('photo-list', [
            'header' => sprintf("Photos by %s", $user->name),
            'photos' => $this->paginate(
                $this->getBuilder()
                ->where('users.id', $user->id)
            ),
        ]);
    }

    public function listByCatalog(User $user, Catalog $catalog)
    {
        return view('photo-list', [
            'header' => sprintf("Catalog %s by %s", $catalog->name, $user->name),
            'photos' => $this->paginate(
                $this->getBuilder()
                ->where('users.id', $user->id)
                ->where('catalogs.id', $catalog->id)
            ),
        ]);
    }

    public function view(Photo $photo)
    {
        return view('photo-view', [
            'photo' => $photo,
            'image' => Helpers::photoPath($photo),
            'user' => User::find($photo->user_id),
            'catalog' => Catalog::find($photo->catalog_id),
            'comments' => DB::table('photo_comments', 'c')
                ->select('c.*', 'u.name as username')
                ->leftJoin('users as u', 'u.id', '=', 'c.user_id')
                ->where('c.photo_id', '=', $photo->id)
                ->orderBy('c.created_at', 'desc')
                ->get()
        ]);
    }

    public function comment(Request $request, Photo $photo)
    {
        $comment = new PhotoComment();
        $comment->comment = $request->get('comment');
        $comment->photo_id = $photo->id;
        if(Auth::check()){
            $comment->user_id = Auth::id();
        }
        $comment->save();

        return Redirect::route('photo.view', ['photo' => $photo])
            ->withFragment('comments');
    }
}
