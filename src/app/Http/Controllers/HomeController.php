<?php

namespace App\Http\Controllers;

use App\Enum\PaginatorEnrichType;
use App\Helpers;
use App\Models\Catalog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    public function list(Catalog $catalog)
    {
        $paginator = DB::table('photos')
            ->where('photos.user_id', Auth::id())
            ->where('photos.catalog_id', $catalog->id)
            ->orderBy('created_at')
            ->paginate(9);

        return view('catalog.photos', [
            'catalog' => $catalog,
            'photos' => Helpers::enrichThumbnailToPaginator($paginator, PaginatorEnrichType::THUMBNAIL),
        ]);
    }
}
