<div>
    <x-input-label>Catalog name <x-dirty-in-label target="form.name" /></x-input-label>
    <x-text-input wire:model.live.debounce.250ms="form.name" />
    <x-validation-error-list />
    @script
    <script>
        $wire.on('catalog-updated', (args) => {
            const elm = document.querySelector('[data-event="catalog-updated"]')
            if(elm && args.length > 0) {
                elm.innerText = args[0].name
            }
        });
    </script>
    @endscript
</div>
