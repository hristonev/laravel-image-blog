<div>
    <h2 class="text-4xl font-extrabold dark:text-white">{{ __('Catalog list') }}</h2>
    @if (session()->has('message'))
        <div class="alert alert-success">
            {{ session('message') }}
        </div>
    @endif
    <div class="p-6 text-gray-900 dark:text-gray-100">
        <form wire:submit.prevent="save">
            <div class="flex flex-row justify-between items-end">
                <div class="w-full">
                    <x-input-label for="catalog-name">New catalog name</x-input-label>
                    <x-text-input type="text" class="w-full" id="catalog-name" wire:model="form.name"/>
                </div>
                <div class="p-4">
                    <x-secondary-button class="same-row-submit" type="submit">Create</x-secondary-button>
                </div>
            </div>
        </form>
        <x-validation-error-list />
    </div>
    <div class="p-6 text-gray-900 dark:text-gray-100">
        @foreach($catalogs as $catalog)
            <div class="flex flex-row justify-between list-row" wire:key="item-{{ $catalog->id }}">
                <div>
                    <x-nav-link :href="route('catalog.edit', ['catalog' => $catalog->id])" wire:navigate>
                        {{ $catalog->name }}
                    </x-nav-link>
                </div>
                <div>
                    <span>
                        Total photos: {{ $catalog->photos }}
                    </span>
                    <x-nav-button
                        wire:click="delete({{ $catalog->id }})"
                        wire:confirm="Are you sure you want to delete this catalog and all it contains?"
                    >
                        <i class="fa fa-trash-can"></i>
                    </x-nav-button>
                </div>
            </div>
        @endforeach
        <div class="p-6 pagination">
            {{ $catalogs->links() }}
        </div>
    </div>
</div>
