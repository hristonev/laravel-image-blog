<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <h1 class="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-6xl dark:text-white">
                {{ $photo->name }}
            </h1>
            <img src="{{ asset($image) }}" alt="{{ $photo->name }}">
            <div class="grid grid-cols-2 p-4">
                <ul class="max-w-md space-y-1 text-gray-500 list-none list-inside dark:text-gray-400">
                    <li>Author: <x-nav-link href="{{ route('user.photos', ['user' => $user->id]) }}">{{ $user->name }}</x-nav-link></li>
                    <li>Catalog: <x-nav-link href="{{ route('user.catalog.photos', ['user' => $user->id, 'catalog' => $catalog->id]) }}">{{ $catalog->name }}</x-nav-link></li>
                    <li>Since: {{ $photo->created_at->format('d.m.Y') }}</li>
                </ul>
                <div class="w-full">
                    <form action="{{ route('photo.comment', ['photo' => $photo->id]) }}" class="flex w-full align-bottom" method="post">
                        @csrf
                        <div class="w-full">
                            <x-text-input name="comment" class="w-full" placeholder="Add a comment..." />
                        </div>
                        <x-secondary-button type="submit">
                            <i class="fa fa-paper-plane"></i>
                        </x-secondary-button>
                    </form>
                </div>
            </div>
            <h2 id="comments" class="text-2xl font-extrabold dark:text-white">Comments for this photo:</h2>
            <ul class="max-w-md space-y-1 text-gray-500 list-none list-inside dark:text-gray-400">
                @foreach($comments as $comment)
                    <li>
                    @if(isset($comment->user_id))
                        <x-nav-link href="{{ route('user.photos', ['user' => $comment->user_id]) }}">{{ '@'. $comment->username }}</x-nav-link>
                    @else
                        Anon
                    @endif
                    : {{ $comment->comment }}</li>
                @endforeach
            </ul>
        </div>
    </div>
</x-app-layout>
