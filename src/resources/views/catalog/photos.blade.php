<x-app-layout>
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-navigation-tab-catalog tab="photos" :catalog="$catalog" />
            <div class="bg-white dark:bg-gray-800 overflow-hidden">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    <div class="grid grid-cols-2 md:grid-cols-3 gap-4">
                        @foreach($photos as $photo)
                            <div class="flex flex-col justify-center items-center">
                                <img class="h-auto max-w-full rounded-lg" src="{{ asset($photo->thumbnail) }}" alt="{{ $photo->name }}">
                                @livewire('photo-view', ['photo' => $photo->id])
                            </div>
                        @endforeach
                    </div>
                    {{ $photos->links() }}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
