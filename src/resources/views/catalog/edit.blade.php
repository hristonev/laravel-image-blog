

<x-app-layout>
    <div class="py-4">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <x-navigation-tab-catalog tab="edit" :catalog="$catalog" />
            <div class="bg-white dark:bg-gray-800 overflow-hidden">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    @livewire('catalog-view', ['catalog' => $catalog])
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
