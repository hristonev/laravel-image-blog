<x-app-layout>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            @if (isset($header))
                <h1 class="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-6xl dark:text-white">
                    {{ $header }}
                </h1>
            @endif
            <div class="grid grid-cols-1 md:grid-cols-4 gap-16">
                @foreach($photos as $photo)
                    <x-photo :photo="$photo"/>
                @endforeach
            </div>
            {{ $photos->links() }}
        </div>
    </div>
</x-app-layout>
