@props(['active','name'])

<li class="me-2">
    @if($active)
        <span aria-current="page" {{ $attributes }} class="inline-block p-4 text-blue-600 border-b-2 border-blue-600 rounded-t-lg active dark:text-blue-500 dark:border-blue-500">{{ $slot }}</span>
    @else
        <a {{ $attributes }} class="inline-block p-4 border-b-2 border-transparent rounded-t-lg hover:text-gray-600 hover:border-gray-300 dark:hover:text-gray-300" >{{ $slot }}</a>
    @endif
</li>
