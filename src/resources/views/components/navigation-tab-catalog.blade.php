@props(['tab','catalog','form'])

<div class="text-sm font-medium text-center text-gray-500 border-b border-gray-200 dark:text-gray-400 dark:border-gray-700">
    <div class="flex justify-between">
        <ul class="flex flex-wrap -mb-px">
            <x-nav-tab :href="route('catalog.edit', ['catalog' => $catalog])" :active="$tab == 'edit'" wire:navigate>Edit catalog</x-nav-tab>
            <x-nav-tab :href="route('catalog.photos', ['catalog' => $catalog])" :active="$tab == 'photos'" wire:navigate>Edit photos</x-nav-tab>
            <x-nav-tab :href="route('catalog.add', ['catalog' => $catalog])" :active="$tab == 'add'" wire:navigate>Add new photos</x-nav-tab>
        </ul>
        <h2 class="text-4xl font-extrabold dark:text-white" data-event="catalog-updated">{{ $catalog->name }}</h2>
    </div>
</div>
