@props(['$errors'])

@if($errors->any())
    <div class="p-6 text-gray-900 dark:text-gray-100 js-error-list">
        @foreach($errors->all() as $error)
            <x-alert-error>{{ $error }}</x-alert-error>
        @endforeach
        @script
        <script>
            window.setInterval(() => {
                const errors = document.querySelectorAll('.js-error-list');
                if(errors.length) {
                    window.setTimeout(() => {
                        errors.forEach((e) => { e.remove() })
                    }, 2000)
                }
            }, 100)
        </script>
        @endscript
    </div>
@endif
