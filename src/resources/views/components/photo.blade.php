<div class="flex flex-col justify-center items-center">
    <x-nav-link href="{{ route('photo.view', ['photo' => $photo->id]) }}">
        <img class="h-auto max-w-full rounded-lg" src="{{ asset($photo->thumbnail) }}" alt="{{ $photo->name }}">
    </x-nav-link>
    <div class="flex justify-between w-full px-4">
        <x-nav-link href="{{ route('user.photos', ['user' => $photo->user_id]) }}">by {{ $photo->author }}</x-nav-link>
        <x-nav-link href="{{ route('user.catalog.photos', ['user' => $photo->user_id, 'catalog' => $photo->catalog_id]) }}">{{ $photo->catalog }}</x-nav-link>
    </div>
</div>
