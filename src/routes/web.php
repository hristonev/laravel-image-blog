<?php

use App\Http\Controllers\PhotoController;
use App\Livewire\CatalogList;
use App\Livewire\CatalogView;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [PhotoController::class, 'list'])
    ->name('welcome');

Route::get('/users/{user}', [PhotoController::class, 'listByUser'])
    ->name('user.photos');

Route::get('/users/{user}/catalogs/{catalog}', [PhotoController::class, 'listByCatalog'])
    ->name('user.catalog.photos');

Route::get('/photos/{photo}', [PhotoController::class, 'view'])
    ->name('photo.view');

Route::post('/photos/{photo}/comment', [PhotoController::class, 'comment'])
    ->name('photo.comment');

Route::view('dashboard', 'dashboard')
    ->middleware(['auth', 'verified'])
    ->name('dashboard');

Route::group(['prefix' => 'catalogs/{catalog}', 'middleware' => ['auth', 'verified']], function () {
    Route::view('/', 'catalog.edit')
        ->name('catalog.edit');
    Route::get('/photos', [PhotoController::class, 'adminList'])
        ->name('catalog.photos');
    Route::view('/add', 'catalog.add')
        ->name('catalog.add');
});

//Route::prefix('catalogs')->group(function () {
//    Route::get('/{id}', [CatalogView::class, 'render'])->name('catalog.view');
//})->middleware(['auth', 'verified']);

Route::view('profile', 'profile')
    ->middleware(['auth'])
    ->name('profile');

require __DIR__.'/auth.php';
